
public class Student extends Person{
	int year;
	String faculty;
	Student(String firstName, String lastName,int age,String faculty, int year) {
		super(firstName, lastName,age);
		this.year=year;
		this.faculty=faculty;
		
	}
	public String toString() {
		return "\nStudent data:\n-> First Name: "+this.firstName+"\n-> Last Name: "+this.lastName+"\n-> Age: "+this.age+
				" years old.\n-> Student at Faculty of "+this.faculty+" in year "+
				this.year+".";}
}