import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Scanner;

//MAIN CLASS
//Ciapa Vasile-Robert

public class main{

	public static void main(String[] args) throws IOException  {
		
		String fileName="out.txt";
		
		InputStreamReader isr=new InputStreamReader(System.in);
		BufferedReader br=new BufferedReader(isr);
		
		System.out.print(" * Chose what type of Person you want to create * \n"+"1.Default Person.\n2.Teacher\n3.Student\n\nEnter the number and press <ENTER> ->");
		
		Scanner s=new Scanner(System.in);
		int x=Integer.parseInt(s.nextLine());
		
		try {
			
			PrintWriter outputStream=new PrintWriter(fileName);
			
		if(x==1) {
			System.out.print("\nFirstName: ");
			String firstName = br.readLine();
			
			System.out.print("LastName: ");
			String lastName=br.readLine();
			
			System.out.print("Age: ");
			int age=Integer.parseInt(s.nextLine());
			
			Person p=new Person(firstName,lastName,age);
			
			outputStream.println(p);
			
			System.out.println(p);
			outputStream.close();
			
		}
		else if(x==2) {
			System.out.print("\nFirstName: ");
			String firstName = br.readLine();
			
			System.out.print("LastName: ");
			String lastName=br.readLine();
			
			System.out.print("Age: ");
			int age=Integer.parseInt(s.nextLine());
			
			System.out.print("University: ");
			String university=br.readLine();
			
			Teacher t=new Teacher(firstName,lastName,age,university);
			
			outputStream.println(t);
			
			System.out.println(t);
			outputStream.close();
		}
		else if(x==3) {
			System.out.print("\nFirstName: ");
			String firstName = br.readLine();
			
			System.out.print("LastName: ");
			String lastName=br.readLine();
			
			System.out.print("Age: ");
			int age=Integer.parseInt(s.nextLine());
			
			System.out.print("Faculty: ");
			String faculty=br.readLine();
			
			System.out.print("Year: ");
			int year=Integer.parseInt(s.nextLine());
			
			Student st=new Student(firstName,lastName,age,faculty,year);
			
			outputStream.println(st);
			
			System.out.println(st);
			outputStream.close();
		}
		else {
			System.out.println("You insert a wrong number...");
		}
		
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println("\nIt was successfully created...");
		}
	
		
	}

