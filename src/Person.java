
public class Person {
	String firstName;
	String lastName;
	int age;
	Person(String firstName,String lastName,int age){
		this.firstName=firstName;
		this.lastName=lastName;
		this.age=age;
	}
	public String toString() {
		return "\nPerson data:\n-> First Name: "+this.firstName+"\n-> Last Name:"+this.lastName+"\n-> Age: "+
	this.age+" years old.";
	}
}
