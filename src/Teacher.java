
public class Teacher extends Person {
	String university;
	
	
	Teacher(String firstName, String lastName,int age,String university) {
		super(firstName, lastName,age);
		this.university=university;
	}
	public String toString() {
		return "\nTeacher data:\n-> First Name: "+this.firstName+"\n-> Last Name: "+this.lastName+"\n-> Age: "+
	this.age+" years old\n-> Teacher at: "+this.university+" University.";
	}
}